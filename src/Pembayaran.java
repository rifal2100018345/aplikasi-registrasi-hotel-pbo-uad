import java.io.FileWriter;
import java.io.IOException;
public class Pembayaran {

    private String nama;
    private int tgl;
    private String alamat;
    private int nohp;
    private int harga;
    private int pil;
    private int pil2;
    Bank bank = new Bank();


    public void setPil(int pil) {
        this.pil = pil;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setTgl(int tgl) {
        this.tgl = tgl;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public void setPil2(int pil2) {
        this.pil2 = pil2;
    }

    public void setNohp(int nohp) {
        this.nohp = nohp;
    }
    public int harga() {
        switch (pil){
            case 1:
                harga = 2500000;
                System.out.println("Fasilitas Kamar : AC + Bathhub + WIFI + KingSize Bed + Private Pool");
                System.out.println("Harga Kamar     : " +harga);
                System.out.println("Pembayaran Setiap Tanggal : " +tgl);
                break;
            case 2:
                harga = 1500000;
                System.out.println("Fasilitas Kamar : AC + WIFI + KingSize Bed + Bathub");
                System.out.println("Harga Kamar     : " +harga);
                System.out.println("Pembayaran Setiap Tanggal : " +tgl);
                break;
            case 3:
                harga = 1000000;
                System.out.println("Fasilitas Kamar : AC + WIFI + KingSize Bed + Shower ");
                System.out.println("Harga Kamar     : " +harga);
                System.out.println("Pembayaran Setiap Tanggal : " +tgl);
                break;
            case 4:
                harga = 500000;
                System.out.println("Fasilitas Kamar : AC + Single Bed + WIFI");
                System.out.println("Harga Kamar     : " +harga);
                System.out.println("Pembayaran Setiap Tanggal : " +tgl);
                break;
            default:
                System.out.println("Pilihan Tidak Tersedia");

        }
        return 0;
    }

    public void cetakStruk() {
        System.out.println("---------- Struk Sewa Kamar Hotel ----------");
        System.out.println("Nama Tamu   : " + nama);
        System.out.println("Alamat Tamu : " + alamat);
        System.out.println("No HP Tamu  : " + nohp);
        harga();
        hitungSisa();
        System.out.println("Sisa uang   : " + bank.getSaldo());

        try {
            FileWriter tulisan = new FileWriter("Struk.txt");
            tulisan.write("---------- Struk Sewa Kamar Hotel ----------");
            tulisan.write("Nama Tamu   : " + nama);
            tulisan.write("Alamat Tamu : " + alamat);
            tulisan.write("No HP Tamu  : " + nohp);
            tulisan.write(hitungSisa());
            tulisan.write("Sisa uang   : " + bank.getSaldo());
            tulisan.close();
        }
        catch (IOException e){
            System.out.println("Terjadi Kesalahan");
            e.printStackTrace();
        }
    }
    public int hitungSisa(){
        int sisa = bank.getSaldo()-this.harga;
        bank.setSaldo(sisa);
        return sisa;
    }
}
