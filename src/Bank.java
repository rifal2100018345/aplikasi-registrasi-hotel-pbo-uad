public class Bank {
    private String no_rekening = "12345678910";
    private int saldo = 10000000;

    public String getNo_rekening() {
        return no_rekening;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
}
