import java.util.Scanner;
public class Tamu extends Data{
    private String nama;
    private String alamat;
    private int nohp;
    private int pil;
    private int harga;
    private int tgl;
    private int pil2;
    Scanner in = new Scanner(System.in);
    Pembayaran bayar = new Pembayaran();

    @Override
    public void Tamu() {
        System.out.println("----------- Masukkan Data Anda -----------");
        System.out.print("Masukkan Nama Anda     : ");
        nama = in.next();
        System.out.print("Masukkan Alamat Anda   : ");
        alamat = in.next();
        System.out.print("Masukkan No.HP Anda    : ");
        nohp = in.nextInt();
        System.out.print("Masukkan Tanggal Masuk :");
        tgl = in.nextInt();
        System.out.print("Masukkan Pilihan Kamar   : ");
        pil = in.nextInt();


        bayar.setAlamat(alamat);
        bayar.setNama(nama);
        bayar.setNohp(nohp);
        bayar.setTgl(tgl);
        bayar.setPil(pil);


    }

    @Override
    public void harga() {
        switch (pil){
            case 1:
                harga = 2500000;
                System.out.println("Fasilitas Kamar : AC + Bathhub + WIFI + KingSize Bed + Private Pool");
                System.out.println("Harga Kamar     : " +harga);
                System.out.println("Pembayaran Setiap Tanggal : " +tgl);
                break;
            case 2:
                harga = 1500000;
                System.out.println("Fasilitas Kamar : AC + WIFI + KingSize Bed + Bathub");
                System.out.println("Harga Kamar     : " +harga);
                System.out.println("Pembayaran Setiap Tanggal : " +tgl);
                break;
            case 3:
                harga = 1000000;
                System.out.println("Fasilitas Kamar : AC + WIFI + KingSize Bed + Shower ");
                System.out.println("Harga Kamar     : " +harga);
                System.out.println("Pembayaran Setiap Tanggal : " +tgl);
                break;
            case 4:
                harga = 500000;
                System.out.println("Fasilitas Kamar : AC + Single Bed + WIFI");
                System.out.println("Harga Kamar     : " +harga);
                System.out.println("Pembayaran Setiap Tanggal : " +tgl);
                break;
            default:
                System.out.println("Pilihan Tidak Tersedia");

        }
    }

    @Override
    public void output() {
        bayar.cetakStruk();
    }
}
